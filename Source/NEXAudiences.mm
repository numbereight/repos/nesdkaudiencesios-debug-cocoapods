//
//  NEXAudiences.m
//  Audiences
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXAudiences.h"

// Xcode locally uses the bottom version, through the framework
// Cocoapods uses the top one, as it generated the header in a different
// place.  Here we use both, so it is flexible for either case.
#if __has_include("Audiences-Swift.h")
#import "Audiences-Swift.h"
#else
#import <Audiences/Audiences-Swift.h>
#endif

@implementation NEXAudiences

+ (NSSet<NEXMembership*>* _Nonnull)currentMemberships {
    return [AudiencesInternal currentMemberships];
}

+ (NSSet<NSString*>* _Nonnull)currentIds {
    NSMutableSet<NSString*>* ids = [NSMutableSet new];
    for (NEXMembership* membership in [NEXAudiences currentMemberships]) {
        [ids addObject:membership.ID];
    }
    return ids;
}

+ (NSSet<NSString*>* _Nonnull)currentExtendedIds {
    NSMutableSet<NSString*>* ids = [NSMutableSet new];
    for (NEXMembership* membership in [NEXAudiences currentMemberships]) {
        [ids addObject:membership.extendedId];
    }

    return ids;
}

+ (NSSet<NSString*>* _Nonnull)currentIabIds {
    NSMutableSet<NSString*>* ids = [NSMutableSet new];
    for (NEXMembership* membership in [NEXAudiences currentMemberships]) {
        for (NEXIABAudience* iabAudience in membership.iabIds) {
            [ids addObject:iabAudience.ID];
        }
    }

    return ids;
}

+ (NSSet<NSString*>* _Nonnull)currentExtendedIabIds {
    NSMutableSet<NSString*>* ids = [NSMutableSet new];
    for (NEXMembership* membership in [NEXAudiences currentMemberships]) {
        for (NEXIABAudience* iabAudience in membership.iabIds) {
            [ids addObject:iabAudience.extendedId];
        }
    }

    return ids;
}

+ (NSString* _Nullable)gender {
    return [NEXNumberEight estimatedGender];
}

+ (void)genderWithCompletion:(void (^)(NSString* _Nullable))completion {
    return [NEXNumberEight estimatedGenderWithCompletion:completion];
}

+ (NSNumber* _Nullable)yearOfBirth {
    return [NEXNumberEight estimatedYearOfBirth];
}

+ (void)yearOfBirthWithCompletion:(void (^)(NSNumber* _Nullable))completion {
    return [NEXNumberEight estimatedYearOfBirthWithCompletion:completion];
}

+ (BOOL)shouldPrintErrors {
    return [AudiencesInternal shouldPrintErrors];
}

+ (void)setShouldPrintErrors:(BOOL)shouldPrintErrors {
    [AudiencesInternal setShouldPrintErrors:shouldPrintErrors];
}

+ (NSDictionary<NSString*, NEXParameters*>*)defaultParameters {
    return [AudiencesInternal defaultParameters];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken*)apiToken {
    [NEXAudiences startRecordingWithApiToken:apiToken
                                  parameters:[NEXAudiences defaultParameters]
                                     onStart:nil];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nullable)apiToken
                        parameters:(NSDictionary<NSString*, NEXParameters*>* _Nonnull)parameters
                           onStart:(NEXAudiencesOnStartCallback _Nullable)onStart {
    if (onStart) {
        [AudiencesInternal startRecordingWithApiToken:apiToken
                                           parameters:parameters
                                              onStart:onStart];
    } else {
        [AudiencesInternal startRecordingWithApiToken:apiToken parameters:parameters];
    }
}

+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nullable)apiToken
                        parameters:(NSDictionary<NSString*, NEXParameters*>* _Nonnull)parameters {
    [AudiencesInternal startRecordingWithApiToken:apiToken parameters:parameters];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken*)apiToken
                           onStart:(NEXAudiencesOnStartCallback _Nullable)onStart {
    [NEXAudiences startRecordingWithApiToken:apiToken
                                  parameters:[NEXAudiences defaultParameters]
                                     onStart:onStart];
}

+ (void)updateTopicsWithParameters:(NSDictionary<NSString*, NEXParameters*>* _Nonnull)parameters {
    [AudiencesInternal updateTopicsWithParameters:parameters];
}

+ (void)pauseRecording {
    [AudiencesInternal pauseRecording];
}

+ (void)stopRecording {
    [AudiencesInternal stopRecording];
}

@end
