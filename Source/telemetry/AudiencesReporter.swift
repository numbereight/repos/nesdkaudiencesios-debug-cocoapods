//
//  AudiencesReporter.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-19.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import Insights
import SQLite

class AudiencesReporter {
    static private let LOG_TAG = "AudiencesReporter"
    static private let REPORTING_DUTY_CYCLE = 1.0
    static private let AUDIENCES_REPORTING_DATE_KEY = "audiences_reporting_date"
    static private let AUDIENCES_REPORTING_ENABLED_KEY = "audiences_reporting_enabled"

    private var apiToken: APIToken? {
        return AudiencesInternal.instance.apiToken
    }
    internal let defaults: UserDefaults

    private let dbHelper = AudiencesDBHelper()
    private let uploader: AudiencesReportUploader
    private let usageCountingQueue = DispatchQueue.global(qos: .background)
    private var isUploading: Bool = false
    private let isUploadingLock = PThreadMutex(type: .normal)
    private var isRunning = true

    private var AUDIENCE_REPORTING_TABLE: String {
        return AudiencesDBHelper.AUDIENCE_REPORTING_TABLE
    }

    private var AUDIENCE_AGES_TABLE: String {
        return AudiencesDBHelper.AUDIENCE_AGES_TABLE
    }

    private var AUDIENCE_DAYS_TABLE: String {
        return AudiencesDBHelper.AUDIENCE_DAYS_TABLE
    }

    internal var shouldTrackToday: Bool {
        let currentDate = AudiencesReporter.getDateStr()

        let reportingDate = self.defaults.string(forKey: AudiencesReporter.AUDIENCES_REPORTING_DATE_KEY) ?? "1970-01-01"
        if reportingDate < currentDate {
            let shouldReport = (Double.random(in: 0 ..< 100) / 100) <= AudiencesReporter.REPORTING_DUTY_CYCLE
            self.defaults.setValue(currentDate, forKey: AudiencesReporter.AUDIENCES_REPORTING_DATE_KEY)
            self.defaults.set(shouldReport, forKey: AudiencesReporter.AUDIENCES_REPORTING_ENABLED_KEY)
        }

        return self.defaults.bool(forKey: AudiencesReporter.AUDIENCES_REPORTING_ENABLED_KEY)
    }

    // Used only in testing.
    internal func overrideShouldTrackToday(value: Bool) {
        let currentDate = AudiencesReporter.getDateStr()

        self.defaults.setValue(currentDate, forKey: AudiencesReporter.AUDIENCES_REPORTING_DATE_KEY)
        self.defaults.set(value, forKey: AudiencesReporter.AUDIENCES_REPORTING_ENABLED_KEY)
    }

    init(uploader: AudiencesReportUploader = AudiencesReportUploader()) {
        // swiftlint:disable force_unwrapping
        self.defaults = UserDefaults(suiteName: "ai.numbereight.audiences")!
        // swiftlint:enable force_unwrapping

        self.uploader = uploader
        self.isRunning = true
    }

    func initialise(memberships: Set<Membership>, date: Date = Date()) -> Bool {
        self.isRunning = true

        self.uploadPendingReports(untilDate: date)
        return self.addAudienceUsageRow(memberships: memberships, incrementCount: false, date: date)
    }

    ///
    /// Helper function used to mimic the behaviour of an atomic boolean.
    ///
    /// - Parameter value: Boolean indicating the new value of `isUploading`.
    /// - Returns: Boolean indiciating if the value of `isUploading` was changed.
    ///
    internal func isUploadingGetAndSet(value: Bool) -> Bool {
        return self.isUploadingLock.sync {
            if self.isUploading != value {
                self.isUploading = value
                return true
            }
            return false
        }
    }

    func stop() {
        self.isRunning = false
    }

    internal func uploadPendingReports(untilDate: Date = Date(), completion: ((Bool) -> Void)? = nil) {
        // Need to set this to false before exiting this function.
        if !self.isUploadingGetAndSet(value: true) {
            NELog.msg(AudiencesReporter.LOG_TAG, debug: "Audience report upload in progress, skipping.")
            completion?(false)
            return
        }

        if !self.isRunning {
            NELog.msg(AudiencesReporter.LOG_TAG, warning: "Cannot upload report as the Audience Reporter is not running.")

            // Need to set this to false before exiting this function.
            // If this command fails, ignore.  No other reports will be uploaded,
            // but can't do much about that.
            _ = self.isUploadingGetAndSet(value: false)
            completion?(false)
            return
        }

        let report = self.fetchReport(untilDate: untilDate)
        NELog.msg(AudiencesReporter.LOG_TAG, debug: "Uploading report.", metadata: ["days_of_usage": report.size, "until": AudiencesReporter.getDateStr(date: untilDate)])

        DispatchQueue.global(qos: .background).async {
            self.uploader.uploadReport(report: report) { error in
                if error == nil {
                    // Clear usage counts
                    NELog.msg(AudiencesReporter.LOG_TAG, verbose: "Successfully uploaded usage counts.")
                    if !self.clearRows(untilDate: untilDate) {
                        NELog.msg(AudiencesReporter.LOG_TAG, warning: "Failed to clear rows after upload.")
                    }
                } else if case .unacceptable = error as? UploadError {
                    // The data is bunk, clear the rows anyway
                    NELog.msg(AudiencesReporter.LOG_TAG, warning: "The server permanently rejected the usage counts.")
                    if !self.clearRows(untilDate: untilDate) {
                        NELog.msg(AudiencesReporter.LOG_TAG, warning: "Failed to clear rows after upload.")
                    }
                } else if let err = error {
                    NELog.msg(AudiencesReporter.LOG_TAG, error: "Failed to upload report.", metadata: ["error": err.localizedDescription])
                }

                // Need to set this to false before exiting this function.
                // If this command fails, ignore.  No other reports will be uploaded,
                // but can't do much about that.
                _ = self.isUploadingGetAndSet(value: false)

                // If no error, it succeeded
                completion?(error == nil)
            }
        }
    }

    public static func performDataRemoval() {
        _ = AudiencesDBHelper().clearAllData()
    }

    private func addAudienceUsageRow(memberships: Set<Membership>, incrementCount: Bool, date: Date) -> Bool {
        guard let db = AudiencesDBHelper.connection else {
            NELog.msg(AudiencesReporter.LOG_TAG, warning: "Failed to get connection while adding audience usage row.")
            return false
        }
        let dateStr = AudiencesReporter.getDateStr(date: date)

        do {
            let tempTable = "\(AUDIENCE_REPORTING_TABLE)_temp"
            try db.transaction(.exclusive) {
                try db.run("""
                    DROP TABLE IF EXISTS \(tempTable);
                """)

                try db.run("""
                    CREATE TEMPORARY TABLE \(tempTable) (
                        date TEXT,
                        audience_id TEXT
                    );
                """)

                let insertTmpStmt = try db.prepare("""
                    INSERT INTO \(tempTable) (audience_id, date) VALUES (?1, ?2);
                """)
                for membership in memberships {
                    try insertTmpStmt.run([membership.id, dateStr])
                }

                try db.run("""
                    -- Insert UUID for current date
                    INSERT OR IGNORE INTO \(AUDIENCE_DAYS_TABLE)
                        (date, uuid)
                        VALUES (?1, ?2)
                """, dateStr, AudiencesDBHelper.randomUUID())

                try db.run("""
                    -- Insert new audiences into ages table
                    INSERT OR IGNORE INTO \(AUDIENCE_AGES_TABLE) (audience_id, first_acquired)
                    SELECT audience_id, date FROM \(tempTable);
                """)

                try db.run("""
                    -- Mark audiences deleted
                    UPDATE \(AUDIENCE_AGES_TABLE)
                        SET deleted_at = ?1
                        WHERE deleted_at IS NULL
                        AND audience_id NOT IN (SELECT audience_id FROM \(tempTable));
                """, dateStr)

                try db.run("""
                    -- Undelete relapsed audiences
                    UPDATE \(AUDIENCE_AGES_TABLE)
                        SET first_acquired = ?1, deleted_at = NULL
                        WHERE deleted_at IS NOT NULL
                        AND audience_id IN (SELECT audience_id FROM \(tempTable));
                """, dateStr)

                try db.run("""
                    -- Increment the count of any existing audiences today
                    UPDATE \(AUDIENCE_REPORTING_TABLE)
                        SET count = count + ?2
                        WHERE audience_id IN (SELECT audience_id FROM \(tempTable))
                        AND date = ?1
                """, dateStr, incrementCount ? 1 : 0)

                try db.run("""
                    -- Add new audiences into reporting table
                    INSERT OR IGNORE INTO \(AUDIENCE_REPORTING_TABLE)
                        (date, audience_id, count, age)
                        SELECT
                            t.date,
                            t.audience_id,
                            ?1,
                            CASE WHEN deleted_at ISNULL THEN
                                JULIANDAY(t.date) - JULIANDAY(first_acquired)
                            ELSE 0
                            END
                        FROM \(tempTable) AS t
                        LEFT JOIN \(AUDIENCE_AGES_TABLE) AS a
                            ON t.audience_id = a.audience_id;
                """, incrementCount ? 1 : 0)

                try db.run("DROP TABLE \(tempTable);")
            }

#if DEBUG
            db.trace {
                NELog.msg(AudiencesReporter.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif

            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(AudiencesReporter.LOG_TAG, error: "An SQLite error occured while inserting audience reporting.",
                      metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(AudiencesReporter.LOG_TAG, error: "Failed while inserting audience reporting.", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    func clearRows(untilDate: Date) -> Bool {
        let currentDate = AudiencesReporter.getDateStr()
        let maxDate = AudiencesReporter.getDateStr(date: untilDate)
        guard let db = AudiencesDBHelper.connection else {
            NELog.msg(AudiencesReporter.LOG_TAG, warning: "Failed to get connection while clearing rows.")
            return false
        }
        do {
            try db.transaction(.immediate) {
                let stmt = try db.prepare("DELETE FROM \(AUDIENCE_REPORTING_TABLE) WHERE date < ?1")
                try stmt.run(maxDate)

                let setUploadedStmt = try db.prepare("""
                    UPDATE \(AUDIENCE_DAYS_TABLE)
                    SET uploaded_at = ?1
                    WHERE date < ?2 AND uploaded_at IS NULL
                """)
                try setUploadedStmt.run(currentDate, maxDate)

                let clearOldRowsStmt = try db.prepare("""
                    DELETE FROM \(AUDIENCE_DAYS_TABLE)
                    WHERE rowid NOT IN (SELECT rowid FROM \(AUDIENCE_DAYS_TABLE) ORDER BY date DESC LIMIT ?1)
                """)
                try clearOldRowsStmt.run(AudiencesDBHelper.MAX_AUDIENCE_DAYS_ENTRIES)
            }

#if DEBUG
            db.trace {
                NELog.msg(AudiencesInternal.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            return true
        } catch let Result.error(message, code, statement) {
            NELog.msg(AudiencesInternal.LOG_TAG, error: "An SQLite error occured while clearning rows", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return false
        } catch let error {
            NELog.msg(AudiencesInternal.LOG_TAG, error: "Failed to clear rows from table", metadata: ["error": error.localizedDescription])
            return false
        }
    }

    /// If tracking audience usage counts for the current day, inserts and increments the count of the `audienceIds` into
    /// the `audienceusage` table, and updates `audienceages` as appropriate.
    /// If it is not tracking audience usage counts for the current day, this does nothing.
    ///
    /// # Example
    /// ```
    /// markAudiencesFetched(audienceIDs: ["NE-1-1", "NE-2-6" "NE-2-3"], date: Date())
    /// ```
    /// - Precondition: Audiences should be started using
    ///   [Audiences.startRecording](x-source-tag://Audiences.startRecording)
    ///
    /// - Parameter memberships: The set of audience memberships the user is currently in.
    ///
    /// - Parameter date: The date to record the usage counts.
    ///
    /// - Returns: Boolean indicating if the row(s) were successfully inserted/updated into the database.
    ///
    func markAudiencesFetched(memberships: Set<Membership>, date: Date = Date()) -> Bool {
        if !self.shouldTrackToday {
            NELog.msg(AudiencesReporter.LOG_TAG, debug: "Not tracking audiences today. Skipping.")
            return false
        } else if !self.isRunning {
            NELog.msg(AudiencesReporter.LOG_TAG, debug: "Not running. Skipping.")
            return false
        }

        // TODO: Add Marker
        return self.addAudienceUsageRow(memberships: memberships, incrementCount: true, date: date)
    }

    /// Fetches the audience usage counts for the provided date (defaults to today).
    ///
    /// - Parameter untilDate: the maximum date to fetch audience usage counts.
    ///
    /// - Returns: List of [AudienceUsageUpload](x-source-tag://AudienceUsageUpload), which can be serialized and
    ///   uploaded to the server.  If an error occurs while retrieving the counts from the database, the empty list is returned.
    ///
    func fetchReport(untilDate: Date = Date()) -> AudiencesUsageReport {
        guard let db = AudiencesDBHelper.connection else {
            NELog.msg(AudiencesReporter.LOG_TAG, error: "Failed to get database connection.")
            return AudiencesUsageReport.blank
        }

        let maxDate = AudiencesReporter.getDateStr(date: untilDate)

        do {
            // Reports should be only for days up until the current date
            // and not include any days that have already been uploaded
            let stmt = try db.prepare("""
                SELECT r.date, r.audience_id, r.count, r.age, d.uuid
                    FROM \(AUDIENCE_REPORTING_TABLE) r
                    INNER JOIN \(AUDIENCE_DAYS_TABLE) d
                        ON d.date = r.date
                    WHERE d.date < ?1
                    AND d.uploaded_at IS NULL
            """, maxDate)

#if DEBUG
            db.trace {
                NELog.msg(AudiencesInternal.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
            let output = AudiencesUsageReport.blank
            while let row = try stmt.failableNext() {
                guard let date = row[0] as? String else { continue }
                guard let audience_id = row[1] as? String else { continue }
                guard let requests = row[2] as? Int64 else { continue }
                guard let age = row[3] as? Int64 else { continue }
                guard let uuid = row[4] as? String else { continue }

                let dayReport = output.findOrCreateDayReport(date: date, uuid: uuid)
                let audienceUsage = AudiencesUsage(id: audience_id, requests: requests, age: age)

                dayReport.append(usage: audienceUsage)
            }
            return output
        } catch let Result.error(message, code, statement) {
            NELog.msg(AudiencesInternal.LOG_TAG, error: "An SQLite error occured while fetching audience usage counts.",
                      metadata: ["code": code, "message": message, "statement": statement.debugDescription])
        } catch let error {
            NELog.msg(AudiencesInternal.LOG_TAG, error: "An error occured while fetching audience usage counts.", metadata: ["error": error.localizedDescription])
        }

        return AudiencesUsageReport.blank
    }

    public static func getDateStr(date: Date = Date()) -> String {
        // Fallback on earlier versions
        let formatter = DateFormatter()
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(abbreviation: "utc")
        formatter.locale = enUSPosixLocale
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .gregorian)

        return formatter.string(from: date)
    }
}
