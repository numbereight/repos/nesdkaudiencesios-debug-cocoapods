//
//  AudiencesUsageDayReport.swift
//  Audiences
//
//  Created by Matthew Paletta on 2022-07-22.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

class AudiencesUsageDayReport: Encodable {
    var date: String
    var uuid: String
    var audiences: [AudiencesUsage]

    var count: Int {
        return self.audiences.count
    }

    init(date: String, uuid: String, audiences: [AudiencesUsage] = []) {
        self.date = date
        self.uuid = uuid
        self.audiences = audiences
    }

    enum CodingKeys: String, CodingKey {
        case date
        case uuid
        case audiences
    }

    func append(usage: AudiencesUsage) {
        self.audiences.append(usage)
    }

    func findAudiencesUsage(id: String) -> AudiencesUsage? {
        return self.audiences.first { $0.id == id }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.date, forKey: .date)
        try container.encode(self.uuid, forKey: .uuid)
        try container.encode(self.audiences, forKey: .audiences)
    }
}
