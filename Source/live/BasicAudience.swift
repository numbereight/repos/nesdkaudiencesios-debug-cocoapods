//
//  BasicAudience.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-25.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import Insights

/**
 * A helper class used to implement matching an audience ID from a `Glimpse` state.
 *
 * - Tag: BasicAudience
 */
struct BasicAudience {
    let name: String
    let id: String
    let topic: String
    let value: NSRegularExpression?
    let iabIds: [IABAudience]

    ///
    /// Default constructor for a [BasicAudience](x-source-tag://BasicAudience)
    ///
    /// - Parameter name: The human-readable representation of the NumberEight audience, stored in `id`.
    /// - Parameter id: The audience ID from the NumberEight audience list.
    /// - Parameter topic: The topic, within the current `Glimpse` state, to apply the regex rule
    ///                    defined in `value`.
    /// - Parameter value: The regex string used to match against the current glimpse state to determine if the
    ///                    user falls into this audience.
    /// - Parameter iab: The list of equivalent IAB Ids that correspond to this NumberEight Audience.
    ///
    /// - Example:
    /// ```
    /// BasicAudience("Walking", "NE-100-2", kNETopicActivity, "*.walking.*", [IABAudience(id: "123", extensions: ["456", "PIFI789"])])
    /// ```
    ///
    init(name: String, id: String, topic: String, value: String, iab: [IABAudience]) {
        self.name = name
        self.id = id
        self.topic = topic
        do {
            self.value = try NSRegularExpression(pattern: value)
        } catch {
            self.value = nil
        }
        self.iabIds = iab
    }
}
