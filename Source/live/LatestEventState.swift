//
//  LatestEventState.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-26.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

///
/// A helper class used for remembering the latest state of a topic.
///
/// - Tag: LatestEventState
///
class LatestEventState: Hashable {

    ///
    /// The serialized representation of the most probable value in a `Glimpse`.
    ///
    public let value: String

    ///
    /// The confidence of the most probable value in a `Glimpse`.
    ///
    public let confidence: Double

    init(value: String, confidence: Double) {
        self.value = value
        self.confidence = confidence
    }

    static func == (lhs: LatestEventState, rhs: LatestEventState) -> Bool {
        return lhs.value == rhs.value && lhs.confidence == rhs.confidence
    }

    func copy() -> LatestEventState {
        return LatestEventState(value: self.value, confidence: self.confidence)
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.value)
        hasher.combine(self.confidence)
    }
}

internal extension Dictionary where Key == String, Value == LatestEventState {
    func copy() -> [Key: Value] {
        var output = [Key: Value]()
        for (k, v) in self {
            output[k] = v.copy()
        }
        return output
    }
}
