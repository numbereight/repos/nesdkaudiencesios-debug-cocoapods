//
//  TimeContextAudience.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-26.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import Insights

///
/// Represents an audience membership that is based on `Glimpse` context. This may translate into a
/// live audience or a post-live audience.
///
/// - Tag: TimeContextAudience
///
struct TimeContextAudience {
    let id: String
    let age: Int

    ///
    /// Maps the age of a historical audience into a `LivenessState` category.
    ///
    /// - Parameter age: The number of days since the user last fell into this audience.
    ///
    /// - Returns: The liveness corresponding to an audience age, or null if an audience is older
    ///            than 30 days.
    ///
    /// - Tag: TimeContextAudience.getLiveness
    ///
    private static func getLiveness(age: Int) -> NEXLivenessState? {
        switch age {
        case 0:
            return .happenedToday
        case 1...7:
            return .happenedThisWeek
        case 8...30:
            return .happenedThisMonth
        default:
            return nil
        }
    }

    ///
    /// Returns a [TimeContextAudience](x-source-tag://TimeContextAudience) to a `Membership`.
    ///
    /// - Returns: The [TimeContextAudience](x-source-tag://TimeContextAudience) mapped into a `Membership` or null if no such mapping
    ///            exists. This may occur if the age is older than the largest liveness
    ///            category (1 month).
    ///
    /// - Tag: TimeContextAudience.toMembership
    ///
    func toMembership() -> Membership? {
        // If we cannot get the liveness, skip over it.
        guard let liveness = TimeContextAudience.getLiveness(age: self.age) else {
            return nil
        }

        guard let audienceDefinition = (kAudienceDefinitions.first {
            $0.id == self.id
        }) else { return nil }

        let audienceName = audienceDefinition.name
        let iabIds = audienceDefinition.iabIds

        // Live context audiences do not have an IAB audience ID.
        return Membership(id: self.id, name: audienceName, iabIds: iabIds, liveness: liveness)
    }
}
