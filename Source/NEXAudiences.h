//
//  NEXAudiences.h
//  Audiences
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Insights/Insights.h>

typedef void (^NEXAudiencesOnStartCallback)(NSError* _Nullable);

NS_SWIFT_NAME(Audiences)
@interface NEXAudiences : NSObject

/**
 * The list of audience memberships detected by NumberEight Audiences.
 * This list changes periodically as the user uses the device more.
 *
 * @return A set of audience memberships for the user containing an ID, audience name, and
 * equivalent IAB Audience Taxonomy IDs. e.g.
 * - Membership("NE-1-1", "Joggers", [ IABAudience("410") ])
 * - Membership("NE-2-6", "Culture Vultures", [ IABAudience("779") ])
 * - Membership("NE-2-3", "Cinema Buffs", [ IABAudience("467"), IABAudience("787", [ "PIFI3" ]) ])
 */
@property(class, readonly) NSSet<NEXMembership*>* _Nonnull currentMemberships NS_REFINED_FOR_SWIFT;

/**
 * The list of audience IDs detected by NumberEight Audiences.
 * This is a convenience function to return only the IDs from the list of memberships.
 *
 * @return A set of audience membership IDs for the user.
 * e.g.
 * - "NE-1-1"
 * - "NE-2-6"
 * - "NE-2-3"
 */
@property(class, readonly) NSSet<NSString*>* _Nonnull currentIds NS_REFINED_FOR_SWIFT;

/**
 * The list of audience IDs and their corresponding liveness detected by NumberEight Audiences.
 * This is a convenience function to return only the extended IDs from the list of memberships.
 *
 * This is useful for use cases such as adding audience memberships to ad requests.
 *
 * @return A set of extended audience membership IDs for the user.
 * e.g.
 * - "NE-1-1|H"
 * - "NE-100-1|L"
 * - "NE-101-2|T"
 */
+ (NSSet<NSString*>* _Nonnull)currentExtendedIds NS_REFINED_FOR_SWIFT;

/**
 * The list of IAB Audience Taxonomy IDs detected by NumberEight Audiences.
 * This is a convenience function to return only the IAB IDs from the list of memberships.
 *
 * This is useful for use cases such as adding audience memberships to ad requests.
 *
 * @return A set of IAB IDs for the user.
 * e.g.
 * - "408"
 * - "762"
 */
+ (NSSet<NSString*>* _Nonnull)currentIabIds NS_REFINED_FOR_SWIFT;

/**
 * The list of extended IAB Audience Taxonomy IDs detected by NumberEight Audiences.
 * This is a convenience function to return the IAB IDs along with any extensions such
 * as purchase intent from the list of memberships.
 *
 * This format is as described by the IAB Seller-Defined Audiences guidance.
 *
 * This is useful for use cases such as adding audience memberships to ad requests.
 *
 * @return A set of IAB IDs for the user.
 * e.g.
 * - "408|PIFI1"
 * - "762|PIFI2|PIPV2"
 */
+ (NSSet<NSString*>* _Nonnull)currentExtendedIabIds NS_REFINED_FOR_SWIFT;

/**
 * Returns the predicted gender for the current user, if available.
 * This will immediately return the current value and will fetch the new value in the background
 * if it has not already been fetched.
 *
 * This is thread safe.
 *
 * e.g. "Male", "Female"
 */
+ (NSString* _Nullable)gender;

/**
 * Asynchronously returns the predicted gender for the current user, if available.
 * This will force refresh the value from the server.
 *
 * If multiple threads try and make a request while one is already underway, only the one
 * request will be made, and all threads will get the same response.
 * This is thread safe.
 *
 * e.g. "Male", "Female"
 */
+ (void)genderWithCompletion:(void (^_Nonnull)(NSString* _Nullable))completion;

/**
 * Returns the predicted year of birth for the current user, if available.
 * This will immediately return the current value and will fetch the new value in the background
 * if it has not already been fetched.
 *
 * This is thread safe.
 *
 * e.g. 2023
 */
+ (NSNumber* _Nullable)yearOfBirth NS_REFINED_FOR_SWIFT;

/**
 * Asynchronously returns the predicted year of birth for the current user, if available.
 * This will force refresh the value from the server.
 *
 * If multiple threads try and make a request while one is already underway, only the one
 * request will be made, and all threads will get the same response.
 * This is thread safe.
 *
 * e.g. 2023
 */
+ (void)yearOfBirthWithCompletion:(void (^_Nonnull)(NSNumber* _Nullable))completion
    NS_REFINED_FOR_SWIFT;

/**
 * Boolean indicating whether Audiences should print errors to the console.
 *
 * @default `true`
 */
@property(class, readwrite) BOOL shouldPrintErrors DEPRECATED_ATTRIBUTE;

/**
 * NumberEight provided default `Parameters`, suitable for most use cases.
 */
@property(class, readonly, nonnull) NSDictionary<NSString*, NEXParameters*>* defaultParameters;

/**
 Starts recording device usage to categorise the user into audiences.

 This will associate an ID with the current device. If NumberEight Insights is in use
 with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
 and stored locally.

 @note Due to this ID, this data will be treated as Personally Identifiable Information
 until future releases remove the need for an ID.

 @param apiToken Token received from NumberEight.start().
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nullable)apiToken;

/**
 Starts recording device usage to categorise the user into audiences.

 This will associate an ID with the current device. If NumberEight Insights is in use
 with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
 and stored locally.

 @note Due to this ID, this data will be treated as Personally Identifiable Information
 until future releases remove the need for an ID.

 @param apiToken Token received from NumberEight.start().
 @param parameters An optional argument which maps a list of topics to `NEXParameters`. This is
                  used internally to make subscriptions to those topics, which are then used
                  or determining live and post-audiences. The default value should be
                  suitable for most use cases.
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nullable)apiToken
                        parameters:(NSDictionary<NSString*, NEXParameters*>* _Nonnull)parameters;

/**
 Starts recording device usage to categorise the user into audiences.

 This will associate an ID with the current device. If NumberEight Insights is in use
 with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
 and stored locally.

 @note Due to this ID, this data will be treated as Personally Identifiable Information
 until future releases remove the need for an ID.

 @param apiToken Token received from NumberEight.start().
 @param onStart Optional callback for determining whether recording started successfully.
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nullable)apiToken
                           onStart:(NEXAudiencesOnStartCallback _Nullable)onStart
    NS_REFINED_FOR_SWIFT;

/**
 Starts recording device usage to categorise the user into audiences.

 This will associate an ID with the current device. If NumberEight Insights is in use
 with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
 and stored locally.

 @note Due to this ID, this data will be treated as Personally Identifiable Information
 until future releases remove the need for an ID.

 @param apiToken Token received from NumberEight.start().
 @param parameters An optional argument which maps a list of topics to `NEXParameters`. This is
                   used internally to make subscriptions to those topics, which are then used
                   or determining live and post-audiences. The default value should be
                   suitable for most use cases.
 @param onStart Optional callback for determining whether recording started successfully.
 */
+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nullable)apiToken
                        parameters:(NSDictionary<NSString*, NEXParameters*>* _Nonnull)parameters
                           onStart:(NEXAudiencesOnStartCallback _Nullable)onStart
    NS_REFINED_FOR_SWIFT;

/**
 * Updates the set of topics and their respective `NEXParameters` used for determining live and
 * post-live audiences.
 *
 * @note If there is no change in topics or `NEXParameters` compared to the existing value,
 *       no action is performed.
 *
 * @param parameters A map of topics to `NEXParameters`. This is used internally to make
 *                   subscriptions to those topics, which are then used for determining live and
 *                   post-live audiences. To revert back to the NumberEight provided defaults,
 *                   use `Audiences.defaultParameters`.
 */
+ (void)updateTopicsWithParameters:(NSDictionary<NSString*, NEXParameters*>* _Nonnull)parameters;

/**
 Finishes recording Audiences for the current app session.
 To start a new app session, call `startRecording` again.

 @see -startRecordingWithApiToken:parameters:onStart:
 */
+ (void)pauseRecording;

/**
 * Pauses any existing Audiences recording whilst preserving the current session.
 * To resume, call `startRecording` again.
 *
 * @see -startRecordingWithApiToken:parameters:onStart:
 */
+ (void)stopRecording;

@end
